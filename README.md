# README #
# Maps API to find nearby Gyms #
O aplicativo realiza uma busca na API do google maps localizando as academias(Gym) mais proximas de voce, mostrando a foto, caso nao contenha ele mostra photo not avaiable, o nome da academia, a distancia e o endereço.

Escolhi dividir o projeto em packages por questao de organizacao, onde model contem as classes POJOs, adapter contem o adapter para preencher as textviews, controlar o onclick para a maps activity e recuperar a distancia para o local selecionado e rest contem a parte do retrofit, o api client e a api interface.

Caso tivesse mais tempo tentaria deixar o carregamento dos itens da lista mais rapido e implementaria a api de rotas para o local selecionado.

Nao implementei testes automatizados pois nao conheco sobre essa parte, caso houvesse mais tempo eu estudaria para implementar, e nao implementei para uma segunda plataforma pois meu conhecimento atualmente é somente em android, estou realizando um curso de iOS porem nao finalizei ainda.