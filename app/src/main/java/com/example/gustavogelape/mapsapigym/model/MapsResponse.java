package com.example.gustavogelape.mapsapigym.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class MapsResponse implements Serializable
{
    @SerializedName("next_page_token")
    private String nextPage;
    @SerializedName("results")
    private List<Localizacao> results;
    @SerializedName("status")
    private String status;

    public String getNextPage() {
        return nextPage;
    }

    public void setNextPage(String nextPage) {
        this.nextPage = nextPage;
    }

    public List<Localizacao> getResults() {
        return results;
    }

    public void setResults(List<Localizacao> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }
}
