package com.example.gustavogelape.mapsapigym.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DistanceResponse implements Serializable
{
    @SerializedName("routes")
    private List<Rotas> routes;

    public DistanceResponse(List<Rotas> routes) {
        this.routes = routes;
    }

    public List<Rotas> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Rotas> routes) {
        this.routes = routes;
    }

    public class Rotas implements Serializable
    {
        @SerializedName("legs")
        private List<Distancia> legs;

        public Rotas(List<Distancia> legs) {
            this.legs = legs;
        }

        public List<Distancia> getLegs() {
            return legs;
        }

        public void setLegs(List<Distancia> legs) {
            this.legs = legs;
        }

        public class Distancia implements Serializable
        {
            @SerializedName("distance")
            private DistanciaTamanho size;

            public Distancia(DistanciaTamanho size) {
                this.size = size;
            }

            public DistanciaTamanho getSize() {
                return size;
            }

            public void setSize(DistanciaTamanho size) {
                this.size = size;
            }

            public class DistanciaTamanho implements Serializable
            {
                @SerializedName("text")
                private String text;

                public DistanciaTamanho(String text) {
                    this.text = text;
                }

                public String getText() {
                    return text;
                }

                public void setText(String text) {
                    this.text = text;
                }
            }
        }
    }
}
