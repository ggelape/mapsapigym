package com.example.gustavogelape.mapsapigym.rest;

import com.example.gustavogelape.mapsapigym.model.DistanceResponse;
import com.example.gustavogelape.mapsapigym.model.MapsResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface
{
    @GET("place/nearbysearch/json?")
    Call<MapsResponse> getAllNearbyGyms(@Query("location") String location, @Query("rankby") String rankby, @Query("type") String types, @Query("keyword") String keyword, @Query("key") String apiKey);

    @GET("directions/json?")
    Call<DistanceResponse> getDistanceToGym(@Query("origin") String userLocation, @Query("destination") String destination, @Query("key") String apiKey);

    @GET("place/nearbysearch/json?")
    Call<MapsResponse> getNextPage(@Query("pagetoken") String pageToken, @Query("key") String apiKey);

}
