package com.example.gustavogelape.mapsapigym.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Fotos implements Serializable
{
    @SerializedName("photo_reference")
    private String photo_reference;

    public Fotos(String photo_reference)
    {
        this.photo_reference = photo_reference;
    }

    public String getPhoto_reference()
    {
        return photo_reference;
    }

    public void setPhoto_reference(String photo_reference)
    {
        this.photo_reference = photo_reference;
    }
}
