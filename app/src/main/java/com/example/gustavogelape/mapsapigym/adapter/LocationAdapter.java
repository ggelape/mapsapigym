package com.example.gustavogelape.mapsapigym.adapter;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.gustavogelape.mapsapigym.MainActivity;
import com.example.gustavogelape.mapsapigym.MapsActivity;
import com.example.gustavogelape.mapsapigym.R;
import com.example.gustavogelape.mapsapigym.model.DistanceResponse;
import com.example.gustavogelape.mapsapigym.model.Localizacao;
import com.example.gustavogelape.mapsapigym.rest.ApiClass;
import com.example.gustavogelape.mapsapigym.rest.ApiInterface;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LocationAdapter extends ArrayAdapter <Localizacao>
{
    //region Declarations
    private List<Localizacao> localizacaos;
    private int rowLayout;
    private Context context;
    String gymUrl;
    ApiInterface apiService = ApiClass.getClient().create(ApiInterface.class);
    String apiKeyPlaces = MainActivity.API_KEY_PLACES;
    String address;
    //endregion

    public LocationAdapter(Context context, int rowLayout, List<Localizacao> localizacaos)
    {
        super(context,rowLayout,localizacaos);
        this.localizacaos = localizacaos;
        this.rowLayout = rowLayout;
        this.context = context;
    }
    @Override
    public int getCount()
    {
        return super.getCount();
    }

    @Override
    public long getItemId(int position)
    {
        return super.getItemId(position);
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent)
    {
        //region View Holder Declarations and handling
        View convertView;
        final LocalizacaoViewHolder holder;

        if (view == null)
        {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_place, parent, false);
            holder = new LocalizacaoViewHolder(convertView);
            convertView.setTag(holder);
        }
        else
        {
            convertView = view;
            holder = (LocalizacaoViewHolder) convertView.getTag();
        }
        //endregion
        //region Get the Pics and Load them
        if(localizacaos.get(position).getPhotos()!=null)
        {
            gymUrl = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=180&maxheight=165&photoreference="+localizacaos.get(position).getPhotos().get(0).getPhoto_reference()+"&key="+apiKeyPlaces;
        }
        else
        {
            gymUrl = "http://cofunction.com/fp-images/NoImage/img_not_available.png";
        }
        Picasso.with(context).cancelRequest(holder.gymPoster);
        Picasso.with(context).load(gymUrl).into(holder.gymPoster);
        //endregion
        holder.gymName.setText(localizacaos.get(position).getName());
        holder.enderecoTextView.setText(localizacaos.get(position).getVicinity());
        //region Get Current Address
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());

        try
        {
            addresses = geocoder.getFromLocation(MainActivity.latitudeAtual, MainActivity.longitudeAtual, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            address = addresses.get(0).getAddressLine(0);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        //endregion
        //region Callback para a distancia
        Call<DistanceResponse> call = apiService.getDistanceToGym(address,localizacaos.get(position).getVicinity() ,apiKeyPlaces);
        call.enqueue(new Callback<DistanceResponse>()
        {
            @Override
            public void onResponse(Call<DistanceResponse> call, Response<DistanceResponse> response) {
                int statusCode = response.code();
                holder.gymDistance.setText(response.body().getRoutes().get(0).getLegs().get(0).getSize().getText());
            }
            @Override
            public void onFailure(Call<DistanceResponse>call, Throwable t)
            {
                // Log error here since request failed
                System.out.println(call.request().url());
                Log.e("TAG MALDITA O RETORNO: ", t.toString());
            }
        });
        //endregion
        holder.cardView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Bundle destino = new Bundle();
                LatLng destination = new LatLng(localizacaos.get(position).getGeometry().getLocation().getLat(), localizacaos.get(position).getGeometry().getLocation().getLng());
                destino.putParcelable("destination", destination);
                Intent intentao = new Intent(getContext(), MapsActivity.class);
                intentao.putExtra("destino42", destino);
                context.startActivity(intentao);
            }
        });
        return convertView;
    }

    public static class LocalizacaoViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout localizacaoLayout;
        TextView gymName;
        TextView gymDistance;
        TextView enderecoTextView;
        ImageView gymPoster;
        CardView cardView;
        public LocalizacaoViewHolder(View v)
        {
            super(v);
            localizacaoLayout = v.findViewById(R.id.localizacaoLayout);
            enderecoTextView = v.findViewById(R.id.addressTextView);
            gymPoster = v.findViewById(R.id.gymPoster);
            gymName = v.findViewById(R.id.gymName);
            gymDistance = v.findViewById(R.id.gymDistance);
            cardView = v.findViewById(R.id.cardView);
        }
    }
}
