package com.example.gustavogelape.mapsapigym.model;


import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Geometria implements Serializable
{
    @SerializedName("location")
    private Localizacion location;

    public Geometria(Localizacion location)
    {
        this.location = location;
    }

    public Localizacion getLocation()
    {
        return location;
    }

    public void setLocation(Localizacion location)
    {
        this.location = location;
    }

    public class Localizacion implements Serializable
    {
        @SerializedName("lat")
        private double lat;

        @SerializedName("lng")
        private double lng;

        public Localizacion(double lat, double lng)
        {
            this.lat = lat;
            this.lng = lng;
        }

        public double getLat()
        {
            return lat;
        }

        public void setLat(double lat)
        {
            this.lat = lat;
        }

        public double getLng()
        {
            return lng;
        }

        public void setLng(double lng)
        {
            this.lng = lng;
        }
    }
}
