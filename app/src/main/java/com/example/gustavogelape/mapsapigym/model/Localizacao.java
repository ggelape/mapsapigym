package com.example.gustavogelape.mapsapigym.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

public class Localizacao implements Serializable
{
    @SerializedName("geometry")
    private Geometria geometry;
    @SerializedName("icon")
    private String icon;
    @SerializedName("name")
    private String name;
    @SerializedName("photos")
    private List<Fotos> photos;
    @SerializedName("rating")
    private Double rating;
    @SerializedName("vicinity")
    private String vicinity;

    public Localizacao(Geometria geometry, String icon, String name, List<Fotos> photos, Double rating, String vicinity)
    {
        this.geometry = geometry;
        this.icon = icon;
        this.name = name;
        this.photos = photos;
        this.rating = rating;
        this.vicinity = vicinity;
    }

    public Geometria getGeometry()
    {
        return geometry;
    }

    public void setGeometry(Geometria geometry)
    {
        this.geometry = geometry;
    }

    public String getIcon()
    {
        return icon;
    }

    public void setIcon(String icon)
    {
        this.icon = icon;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public List<Fotos> getPhotos()
    {
        return photos;
    }

    public void setPhotos(List<Fotos> photos)
    {
        this.photos = photos;
    }

    public Double getRating()
    {
        return rating;
    }

    public void setRating(Double rating)
    {
        this.rating = rating;
    }

    public String getVicinity()
    {
        return vicinity;
    }

    public void setVincinity(String vicinity)
    {
        this.vicinity = vicinity;
    }

}
