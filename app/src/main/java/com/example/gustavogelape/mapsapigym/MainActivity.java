package com.example.gustavogelape.mapsapigym;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import com.example.gustavogelape.mapsapigym.adapter.LocationAdapter;
import com.example.gustavogelape.mapsapigym.model.Localizacao;
import com.example.gustavogelape.mapsapigym.model.MapsResponse;
import com.example.gustavogelape.mapsapigym.rest.ApiClass;
import com.example.gustavogelape.mapsapigym.rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
{
    //region Declarations
    public static final String API_KEY_PLACES = "AIzaSyAlcrglSjxM_zM9w-ogMUNUqnisc_mTGRs";
    public static final String RANKBY = "distance";
    public static final String TYPES = "gym";
    public static final String KEYWORD = "gym";
    public static double latitudeAtual;
    public static double longitudeAtual;
    private EndlessScrollListener scrollListener;
    //API KEY FOR DIRECTIONS AIzaSyDr94IGpm8YZZLAKl0137VtJAkzCc6wwcg
    ApiInterface apiService = ApiClass.getClient().create(ApiInterface.class);
    ListView listView;
    LocationAdapter adapter;
    String nextPageToken;
    //endregion
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listViewMain);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        else
        {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            // get the last know location from your location manager.
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (location == null)
                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            latitudeAtual = location.getLatitude();
            longitudeAtual = location.getLongitude();
            // now get the lat/lon from the location and do something with it.
            Call<MapsResponse> call = apiService.getAllNearbyGyms(String.valueOf(location.getLatitude()) + "," + String.valueOf(location.getLongitude()), RANKBY, TYPES, KEYWORD, API_KEY_PLACES);
            call.enqueue(new Callback<MapsResponse>() {
                @Override
                public void onResponse(Call<MapsResponse> call, Response<MapsResponse> response) {
                    int statusCode = response.code();
                    List<Localizacao> localizacaos = response.body().getResults();
                    nextPageToken = response.body().getNextPage();
                    adapter = new LocationAdapter(getApplicationContext(), R.layout.list_item_place, localizacaos);
                    listView.setAdapter(adapter);
                }

                @Override
                public void onFailure(Call<MapsResponse> call, Throwable t) {
                    // Log error here since request failed
                    System.out.println(call.request().url());
                    Log.e("OLA TAG MALDITA: ", t.toString());
                }
            });

            scrollListener = new EndlessScrollListener(5) {
                @Override
                public void onLoadMore(int page, int totalItemsCount) {
                    loadMoreData(page);
                }
            };
            listView.setOnScrollListener(scrollListener);
        }
    }

    public void loadMoreData(int page)
    {

        Call<MapsResponse> call = apiService.getNextPage(nextPageToken,API_KEY_PLACES);
        call.enqueue(new Callback<MapsResponse>()
        {

            @Override
            public void onResponse(Call<MapsResponse>call, Response<MapsResponse> response)
            {
                int statusCode = response.code();
                adapter.addAll(response.body().getResults());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<MapsResponse>call, Throwable t)
            {
                // Log error here since request failed
                Log.e("TAG MALDITA PT 3", t.toString());
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
        {
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            {
                LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                // get the last know location from your location manager.
                Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                System.out.println(location);
                latitudeAtual = location.getLatitude();
                longitudeAtual = location.getLongitude();
                // now get the lat/lon from the location and do something with it.
                Call<MapsResponse> call = apiService.getAllNearbyGyms(String.valueOf(location.getLatitude()) + "," + String.valueOf(location.getLongitude()), RANKBY, TYPES, KEYWORD, API_KEY_PLACES);
                call.enqueue(new Callback<MapsResponse>() {
                    @Override
                    public void onResponse(Call<MapsResponse> call, Response<MapsResponse> response) {
                        int statusCode = response.code();
                        List<Localizacao> localizacaos = response.body().getResults();
                        nextPageToken = response.body().getNextPage();
                        adapter = new LocationAdapter(getApplicationContext(), R.layout.list_item_place, localizacaos);
                        listView.setAdapter(adapter);
                    }

                    @Override
                    public void onFailure(Call<MapsResponse> call, Throwable t) {
                        // Log error here since request failed
                        System.out.println(call.request().url());
                        Log.e("OLA TAG MALDITA: ", t.toString());
                    }
                });

                scrollListener = new EndlessScrollListener(5) {
                    @Override
                    public void onLoadMore(int page, int totalItemsCount) {
                        loadMoreData(page);
                    }
                };
                listView.setOnScrollListener(scrollListener);
            }
        }
    }
}


